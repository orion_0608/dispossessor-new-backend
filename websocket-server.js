const express = require("express");
const http = require("http");
var moment = require("moment");
const app = express();
const server = http.createServer(app);
const { Server } = require("socket.io");
const cors = require("cors");
var connection = require("./routes/database");
app.use(cors());

function getUTCCurrentDate(strDate) {
  var currentDate = new Date(strDate);
  if (isNaN(currentDate.getTime())) currentDate = new Date();

  var now = new Date(
    currentDate.getUTCFullYear(),
    currentDate.getUTCMonth(),
    currentDate.getUTCDate(),
    currentDate.getUTCHours(),
    currentDate.getUTCMinutes(),
    currentDate.getUTCSeconds(),
    currentDate.getUTCMilliseconds()
  );

  return now;
}

function getCorrectDateTime(strDate) {
  let dateMoment = moment(getUTCCurrentDate(""));
  if (strDate.length < 9) {
    return dateMoment;
  }

  strDate = strDate.replace("Deadline: ", "");
  if (strDate[0] == '"') {
    strDate = strDate.substr(1, strDate.length - 2);
  }

  if (strDate.length <= 10) {
    if (strDate.length == 10) {
      dateMoment = moment.utc(strDate, "DD.MM.YYYY");
    } else {
      dateMoment = moment.utc(strDate, "D.MM.YYYY");
    }
  } else {
    dateMoment = moment.utc(strDate, "DD MMM, YYYY HH:mm UTC", true);
    if (!dateMoment.isValid()) {
      dateMoment = moment.utc(strDate, "DD MMM, YYYY, HH:mm UTC", true);
      if (!dateMoment.isValid()) {
        dateMoment = moment.utc(strDate, "MMM D, YYYY hh:mm A", true);
        if (!dateMoment.isValid()) {
          dateMoment = moment.utc(strDate, "DD MMM, YYYY HH:mm:ss UTC", true);
          if (!dateMoment.isValid()) {
            dateMoment = moment.utc(strDate, "YYYY-MM-DD HH:mm:ss", true);
          }
        }
      }
    }
  }

  return dateMoment;
}

connection.connect(err => {
  if (err) {
    console.error("Error connecting to the database:", err);
    return;
  }
  console.log("Connected to the database.");
});

const io = new Server(server, {
  cors: {
    origin: "*",
    methods: ["GET", "POST"]
  }
});

io.on("connection", socket => {
  console.log("New client connected");

  // Handle chat message events
  socket.on("chat message", message => {
    console.log("Received message:", message);

    const { chatlink, msg, user } = message;

    connection.query(
      "INSERT INTO chats (username, chatlink, message, createdTime) VALUES (?, ?, ?, ?)",
      [
        user,
        chatlink,
        msg,
        getCorrectDateTime("").format("DD MMM, YYYY HH:mm:ss:ss") + " UTC"
      ],
      async function(error, results) {
        if (error) {
          return;
        }
        io.emit(`${chatlink}`, {
          id: results.insertId,
          message: msg,
          chatlink,
          username: user,
          createdTime:
            getCorrectDateTime("").format("DD MMM, YYYY HH:mm:ss:ss") + " UTC"
        });
      }
    );
  });

  // Handle disconnection
  socket.on("disconnect", () => {
    console.log("Client disconnected");
  });
});

server.listen(3003, () => {
  console.log("Server running on port 3003");
});
