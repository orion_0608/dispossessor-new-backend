(function ($) {
  // Number of seconds in every time division
  var days = 24 * 60 * 60,
    hours = 60 * 60,
    minutes = 60;

  // Creating the plugin
  $.fn.countup = function (prop) {
    var options = $.extend(
      {
        callback: function () {},
        start: new Date("2019-09-03"),
      },
      prop
    );

    var passed = 0,
      d,
      h,
      m,
      s,
      positions;

    // Initialize the plugin
    init(this, options);

    positions = this.find(".position");

    (function tick() {
      passed = Math.floor((new Date() - options.start) / 1000);

      // Number of days passed
      d = Math.floor(passed / days);
      updateDuo(d, 2, 1, 0);
      passed -= d * days;

      // Number of hours left
      h = Math.floor(passed / hours);
      updateDuo(h, 4, 3);
      passed -= h * hours;

      // Number of minutes left
      m = Math.floor(passed / minutes);
      updateDuo(m, 6, 5);
      passed -= m * minutes;

      // Number of seconds left
      s = passed;
      updateDuo(s, 8, 7);

      // Calling an optional user supplied callback
      options.callback(d, h, m, s);

      // Scheduling another call of this function in 1s
      setTimeout(tick, 1000);
    })();

    // This function updates two digit positions at once
    function updateDuo(value, zeros, tens, hundreds) {
      var value100 = Math.floor(value / 100) % 10;
      if ($.isNumeric(hundreds)) {
        switchDigit(positions.eq(hundreds), value100);
        $(".hundreds-of-days").toggle(value100 > 0);
      }
      switchDigit(positions.eq(tens), Math.floor(value / 10) % 10);
      switchDigit(positions.eq(zeros), value % 10);
    }

    return this;
  };

  function init(elem, options) {
    elem.addClass("countdownHolder");

    // Creating the markup inside the container
    $.each(["Days", "Hours", "Minutes", "Seconds"], function (i) {
      $('<span class="count' + this + '">')
        .html(
          '<span class="position">\
                    <span class="digit static">0</span>\
                </span>\
                <span class="position">\
                    <span class="digit static">0</span>\
                </span>'
        )
        .appendTo(elem);

      if (this != "Seconds" && this == "Days") {
        elem.append(
          '<span class="time-label">D</span><span class="countDiv countDiv' +
            i +
            '"></span>'
        );
      }
      if (this != "Seconds" && this == "Hours") {
        elem.append(
          '<span class="time-label">H</span><span class="countDiv countDiv' +
            i +
            '"></span>'
        );
      }
      if (this != "Seconds" && this == "Minutes") {
        elem.append(
          '<span class="time-label">M</span><span class="countDiv countDiv' +
            i +
            '"></span>'
        );
      }
      if (this == "Seconds") {
        elem.append('<span class="time-label">S</span>');
      }
    });
    $(
      '<span class="position hundreds-of-days"><span class="digit static"></span></span><span></span>'
    ).prependTo(".countDays");
  }

  // Creates an animated transition between the two numbers
  function switchDigit(position, number) {
    var digit = position.find(".digit");

    if (digit.is(":animated")) {
      return false;
    }

    if (position.data("digit") == number) {
      // We are already showing this number
      return false;
    }

    position.data("digit", number);

    var replacement = $("<span>", {
      class: "digit",
      css: {
        top: "-2.1em",
        opacity: 0,
      },
      html: number,
    });

    // The .static class is added when the animation
    // completes. This makes it run smoother.

    digit
      .before(replacement)
      .removeClass("static")
      .animate({ top: "2.5em", opacity: 0 }, "fast", function () {
        digit.remove();
      });

    replacement.delay(100).animate({ top: 0, opacity: 1 }, "fast", function () {
      replacement.addClass("static");
    });
  }
})(jQuery);

/*Another countup script*/
window.onload = function () {
  // Month Day, Year Hour:Minute:Second, id-of-element-container
  countUpFromTime("Sep 3, 2019 12:00:00", "countup1"); // ****** Change this line!
};

function countUpFromTime(countFrom, id) {
  countFrom = new Date(countFrom).getTime();
  var now = new Date(),
    countFrom = new Date(countFrom),
    timeDifference = now - countFrom;

  var secondsInADay = 60 * 60 * 1000 * 24,
    secondsInAHour = 60 * 60 * 1000;

  days = Math.floor((timeDifference / secondsInADay) * 1);
  years = Math.floor(days / 365);
  if (years > 1) {
    days = days - years * 365;
  }
  hours = Math.floor(((timeDifference % secondsInADay) / secondsInAHour) * 1);
  mins = Math.floor(
    (((timeDifference % secondsInADay) % secondsInAHour) / (60 * 1000)) * 1
  );
  secs = Math.floor(
    ((((timeDifference % secondsInADay) % secondsInAHour) % (60 * 1000)) /
      1000) *
      1
  );

  var idEl = document.getElementById(id);
  idEl.getElementsByClassName("years")[0].innerHTML = years;
  idEl.getElementsByClassName("days")[0].innerHTML = days;
  idEl.getElementsByClassName("hours")[0].innerHTML = hours;
  idEl.getElementsByClassName("minutes-countup")[0].innerHTML = mins;
  idEl.getElementsByClassName("seconds-countup")[0].innerHTML = secs;

  clearTimeout(countUpFromTime.interval);
  countUpFromTime.interval = setTimeout(function () {
    countUpFromTime(countFrom, id);
  }, 1000);
}
