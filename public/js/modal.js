$().ready(function () {
  function overflowHide() {
    $(document.body).css("overflow", "hidden");
  }
  function overflowShow() {
    $(document.body).css("overflow", "");
  }

  $(document.body).on("click", "#buy_datadownload_btn", function (e) {
    $("#modal_chat_blog_download").show();
    overflowHide();
  });

  $(document.body).on("click", ".close_modal", function (e) {
    $(this).parents(".modal").hide();
    overflowShow();
  });
  $(document.body).on("click", ".close-modal-btn", function (e) {
    $(this).parents(".modal").hide();
    overflowShow();
  });

  $(document.body).on("click", "#buy_more_time_btn", function (e) {
    $("#modal_pay24_blog").show();
    overflowHide();
  });

  $(document.body).on("click", "#buy_destroy_btn", function (e) {
    $("#modal_chat_blog_destroy").show();
    overflowHide();
  });

  $(document).keyup(function (e) {
    if (e.keyCode === 27) {
      // escape key maps to keycode `27`
      $(".modal").hide();
      window.location.href = "#1";
      overflowShow();
    }
  });

  var scrollBarWidth = window.innerWidth - $(window).width();
  for (let i = 1; i < 5; i++) {
    $(document.body).on("click", "#btc_" + i, function (e) {
      $(document.body).css("padding-right", scrollBarWidth + "px");
      $(".modal-slide-up-down").show();
      $("#modal" + i).addClass("modal-slide-up-down" + i + "-active");
      $(".modal-content").slideDown();
      overflowHide();
    });
  }
});
