jQuery(function () {
    $('#txtUploadedDate').datetimepicker({
        lang: 'en',
        timepicker: false,
        format: 'Y-m-d H:i:s'
    });

    $('#txtUpdatedDate').datetimepicker({
        lang: 'en',
        timepicker: false,
        format: 'Y-m-d H:i:s'
    });

    $('#txtDeadlineDate').datetimepicker({
        lang: 'en',
        timepicker: false,
        format: 'Y-m-d H:i:s'
    });
});

var currentDate = new Date();
var currentDateTimeString = currentDate.dateFormat('Y-m-d H:i:s');

txtBoxUploadedDate = document.getElementById("txtUploadedDate");
txtBoxUploadedDate.value = currentDateTimeString;
txtBoxUploadedDate.style.color = "gray";
txtBoxUploadedDate.addEventListener("focus", function () {
    txtBoxUploadedDate.style.color = "black";
});
txtBoxUpdatedDate = document.getElementById("txtUpdatedDate");
txtBoxUpdatedDate.value = currentDateTimeString;
txtBoxUpdatedDate.style.color = "gray";
txtBoxUpdatedDate.addEventListener("focus", function () {
    txtBoxUpdatedDate.style.color = "black";
});

$("#isPublished")[0].checked = false;
$("#isPublished").on("change", function (evt) {
    let flag = evt.target.checked;
    $("#txtDeadlineDate").prop("disabled", flag);
});

// $('#btnAddBlock').on('click', function (event) {
$('#uploadForm').submit(function(event) {
    event.preventDefault(); // Prevent default form submission
    
    var formDataArray = $('form').serializeArray();

    // Convert the array of objects into a single object
    var formDataObject = {};
    $.each(formDataArray, function (index, element) {
        formDataObject[element.name] = element.value;
    });

    let date = new Date(formDataObject.txtUploadedDate);
    if (!isNaN(date.getTime()))
        formDataObject.txtUploadedDate = date.toISOString().replace('T', ' ').slice(0, 19);
    date = new Date(formDataObject.txtUpdatedDate);
    if (!isNaN(date.getTime()))
        formDataObject.txtUpdatedDate = date.toISOString().replace('T', ' ').slice(0, 19);
    date = new Date(formDataObject.txtDeadlineDate);
    if (!isNaN(date.getTime()))
        formDataObject.txtDeadlineDate = date.toISOString().replace('T', ' ').slice(0, 19);

    var formData = new FormData(this);
    // var fileLogo = $('#fileCompanyLogo').prop('files')[0];
    // if (fileLogo != undefined)
    //     formData.append('fileCompanyLogo', fileLogo);
    for (var key in formDataObject) {
        formData.set(key, formDataObject[key])
    }

    // Construct the base URL
    var URL = window.location.protocol + '//' + window.location.host + '/addpost/add';

    $.ajax({
        url: URL,
        type: 'POST',
        data: formData,
        processData: false,
        contentType: false,
        success: function (response) {
            console.log(response);
            if (response.message == 'sign-out') {
                location.reload();
                return;
            }

            if (response.message != undefined) {
                Swal.fire({
                    title: 'Add Block',
                    text: response.message.content,
                    icon: response.message.type,
                    confirmButtonText: 'OK'
                });
    
                if (response.message.type == 'success')
                    $('#uploadForm')[0].reset();
            }
        },
        error: function (xhr, status, error) {
            console.log(error);
            Swal.fire({
                title: 'Add Block',
                text: error,
                icon: 'error',
                confirmButtonText: 'OK'
            });
        }
    });
});