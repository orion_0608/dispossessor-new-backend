$(document).ready(function () {
  var offset = 0;
  var filesCntTotal = 0;
  var filesCntOnPage = 0;
  var lastFilesCntLoaded = 0;
  var currentPath = "";
  var onPageLimit = 14;

  const Toast = Swal.mixin({
    toast: true,
    position: "top",
    showConfirmButton: false,
    timer: 2000,
    timerProgressBar: true,
    onOpen: (toast) => {
      toast.addEventListener("mouseenter", Swal.stopTimer);
      toast.addEventListener("mouseleave", Swal.resumeTimer);
    },
  });

  $(document).ajaxStart(function () {
    $(".file-listing__wrapper").addClass("loading");
  });
  $(document).ajaxStop(function () {
    $(".file-listing__wrapper").removeClass("loading");
  });
  $(document).ajaxError(function () {
    $(".file-listing__wrapper").removeClass("loading");
  });

  function updatePagination(listingPage, totalAmount) {
    var $curFiles = listingPage * onPageLimit;
    setFilesCounter($curFiles, totalAmount);
  }

  var $listing_id = 3;

  if ($(".file-listing__wrapper").length) {
    openPath("");
  }

  $("body").on(
    "dblclick",
    ".file-listing__item .file-listing__name, .file-listing__item .file-listing__date",
    function (e) {
      e.preventDefault();
      showDownloadSingleFile(false);

      var path = $(this).parent().attr("data-dir");
      if (path) {
        openPath(path);
      }
    }
  );

  $("body").on(
    "click",
    ".file-listing__item .file-listing__name, .file-listing__item .file-listing__date",
    function (e) {
      if (
        !$(this).parent().hasClass("active") &&
        $("#listing-download-single-file").length
      ) {
        $(".file-listing__item").removeClass("active");
        $(this).parent().addClass("active");
        var elem = $(this);
        if (
          elem
            .parent()
            .children(".file-listing__name")
            .hasClass("file__name--folder")
        ) {
          showDownloadSingleFile(false);
        } else {
          postJSON(
            "/ajax/listing-post",
            {
              explorer: true,
              file_chose: true,
              post: slug,
            },
            function (data) {
              showDownloadSingleFile(data.message);
            }
          );
        }
      }
    }
  );

  $(".next-one-page").on("click", function (e) {
    e.preventDefault();
    if ($(this).hasClass("disabled")) return;
    offset++;
    showDownloadSingleFile(false);
    postJSON(
      "/ajax/listing-post",
      {
        explorer: true,
        path: currentPath,
        split_idx: offset,
        post: slug,
      },
      function (data) {
        var response = data.message;
        filesCntOnPage += response["file_list"].length;
        setFilesCounter(filesCntOnPage, response["files_amount"]);
        lastFilesCntLoaded = response["file_list"].length;
        $(".file-listing__content[data-listing='" + $listing_id + "']")
          .children()
          .remove();
        $(".file-listing__content[data-listing='" + $listing_id + "']").append(
          response["file_list"]
        );
      }
    );
  });

  $(".back-one-page").on("click", function (e) {
    e.preventDefault();
    if ($(this).hasClass("disabled")) return;
    offset--;
    showDownloadSingleFile(false);
    postJSON(
      "/ajax/listing-post",
      {
        explorer: true,
        path: currentPath,
        split_idx: offset,
        post: slug,
      },
      function (data) {
        var response = data.message;
        filesCntOnPage -= lastFilesCntLoaded;
        setFilesCounter(filesCntOnPage, response["files_amount"]);
        lastFilesCntLoaded = response["file_list"].length;
        $(".file-listing__content[data-listing='" + $listing_id + "']")
          .children()
          .remove();
        $(".file-listing__content[data-listing='" + $listing_id + "']").append(
          response["file_list"]
        );
      }
    );
  });

  $(".on-end").on("click", function (e) {
    e.preventDefault();
    if ($(this).hasClass("disabled")) return;
    offset = filesCntTotal / onPageLimit;
    if (filesCntTotal % onPageLimit == 0) {
      offset--;
    }
    showDownloadSingleFile(false);
    postJSON(
      "/ajax/listing-post",
      {
        explorer: true,
        child_dir: true,
        path: currentPath,
        split_idx: offset,
        post: slug,
      },
      function (data) {
        var response = data.message;
        setFilesCounter(response["files_amount"], response["files_amount"]);
        filesCntOnPage = response["files_amount"];
        lastFilesCntLoaded = response["file_list"].length;
        $(".file-listing__content[data-listing='" + $listing_id + "']")
          .children()
          .remove();
        $(".file-listing__content[data-listing='" + $listing_id + "']").append(
          response["file_list"]
        );
      }
    );
  });

  $(".on-start").on("click", function (e) {
    e.preventDefault();
    if ($(this).hasClass("disabled")) return;
    showDownloadSingleFile(false);
    postJSON(
      "/ajax/listing-post",
      {
        explorer: true,
        child_dir: true,
        path: currentPath,
        split_idx: 0,
        post: slug,
      },
      function (data) {
        var response = data.message;
        $(".file-listing__content[data-listing='" + $listing_id + "']")
          .children()
          .remove();
        $(".file-listing__content[data-listing='" + $listing_id + "']").append(
          response["file_list"]
        );
        offset = 0;
        lastFilesCntLoaded = response["file_list"].length;
        filesCntOnPage = lastFilesCntLoaded;
        setFilesCounter(lastFilesCntLoaded, response["files_amount"]);
      }
    );
  });

  $(".single-file-download-btn").on("click", function (e) {
    e.preventDefault();

    var path = $(".file-listing__item.active").attr("data-dir");
    showDownloadSingleFile(false);
    postJSON(
      "/ajax/listing-post",
      {
        explorer: true,
        "file-download": true,
        path: path,
        post: slug,
      },
      function (data) {
        var response = data.message;
        if (!data.error) {
          downloadFile(
            "/ajax/listing-post?file-download=true&post=" +
              slug +
              "&path=" +
              encodeURIComponent(path),
            response["filename"]
          );
        } else {
          Toast.fire({
            icon: "error",
            title: response,
          });
        }
      }
    );
  });

  function downloadFile(path, filename = null) {
    Toast.fire({
      icon: "success",
      title: "Downloading " + (filename ?? ""),
    });

    var link = document.createElement("a");
    link.setAttribute("href", path);
    link.setAttribute("target", "_blank");
    link.click();
  }

  $(".file-download-btn").on("click", function (e) {
    e.preventDefault();

    if ($(this).hasClass("nonactive")) {
      return;
    }
    var filename = $(this).data("filename");

    downloadFile($(this).attr("href"), filename);
  });

  $(".file__back").on("click", function (e) {
    e.preventDefault();
    var $that = $(this);
    if (!$(this).hasClass("active")) return;
    let currentPathArr = currentPath.split("/");
    currentPathArr.pop();
    path = currentPathArr.join("/");
    if (path !== "") {
      $that.addClass("active");
    } else {
      $that.removeClass("active");
    }
    openPath(path);
  });

  $("#open_chat_btn").on("click", function (e) {
    post("/ajax/get-chat", { slug: slug }, function (data) {
      $("#modal_open_chat").show();
      $(document.body).css("overflow", "hidden");
      $("#modal_open_chat .modal-content").html("");
      $("#modal_open_chat .modal-content").append(data);
    });
  });

  function setFilesCounter(onPage, total) {
    filesCntTotal = parseInt(total);
    $(".current").text(onPage + "/" + total);
    if (onPage <= onPageLimit) {
      $(".back-one-page,.on-start").addClass("disabled");
    } else {
      $(".back-one-page,.on-start").removeClass("disabled");
    }
    if (onPage >= total) {
      $(".next-one-page,.on-end").addClass("disabled");
    } else {
      $(".next-one-page,.on-end").removeClass("disabled");
    }
  }

  function showDownloadSingleFile(r) {
    if (!r) {
      $(".single-file-download-btn").addClass("nonactive");
      //$('#listing-download-single-file').hide();
    } else {
      $(".single-file-download-btn").removeClass("nonactive");
      $("#listing-download-single-file").show();
    }
  }

  $(document.body).on("click", "#listing_full_path span", function (e) {
    $("#listing_full_path span").each(function (k, v) {
      $(v).attr("data-index", k);
    });
    e.preventDefault();

    let index = $(this).data("index");
    var pathSplit = [];
    $("#listing_full_path span").each(function (k, v) {
      if (index >= $(v).data("index")) {
        pathSplit.push($(v).text());
      }
    });

    openPath(pathSplit.join("/"));
  });

  function openPath(path) {
    showDownloadSingleFile(false);
    $("#listing_full_path").html(
      "<span>" + path.split("/").join("</span> / <span>") + "</span>"
    );

    postJSON(
      "/ajax/listing-post",
      {
        explorer: true,
        post: slug,
        path: path,
      },
      function (data) {
        if (path == "") {
          {
            var response = data.message;
            if (response["file_list"]) {
              $(".file-listing__wrapper").show();
              $(".file-listing__content[data-listing='" + $listing_id + "']")
                .children()
                .remove();
              $(
                ".file-listing__content[data-listing='" + $listing_id + "']"
              ).append(response["file_list"]);
            }

            $(".publication-timer").attr("data-timer", response["timer"]);
            $(".publication-timer").each(function (k, timerBlock) {
              let date = timerBlock.attr("data-timer");
              timer(date, timerBlock);
            });

            filesCntOnPage = response["file_list"].length;
            setFilesCounter(filesCntOnPage, response["files_amount"]);

            $(".single-file-download-btn").addClass("nonactive");
            if (response["can_download"]) {
              $("button.single-file-download-btn").removeClass("nonactive");
            } else {
              $("button.single-file-download-btn").addClass("nonactive");
            }
          }
        } else {
          currentPath = path;
          offset = 0;
          filesCntOnPage = 0;
          var response = data.message;
          $(".file__back[data-listing='" + $listing_id + "']").addClass(
            "active"
          );
          $(".file-listing__content[data-listing='" + $listing_id + "']")
            .children()
            .remove();
          $(
            ".file-listing__content[data-listing='" + $listing_id + "']"
          ).append(response["file_list"]);
          updatePagination(
            response["file_list"].length,
            response["files_amount"]
          );
          filesCntOnPage += response["file_list"].length;
          setFilesCounter(filesCntOnPage, response["files_amount"]);
        }
      }
    );
  }
});
