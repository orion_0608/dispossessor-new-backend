$().ready(function () {
    $('#btnCloseTicket').on('click', function (event) {
        Swal.fire({
            title: 'Confirmation',
            text: "Are you sure you want to close this support ticket?",
            icon: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes',
            cancelButtonText: 'No',
        }).then((result) => {
            if (result.isConfirmed) {
                // Construct the base URL
                var URL = window.location.protocol + '//' + window.location.host + '/ticket/close/' + $("input[name=ticketID]").val();
            
                $.ajax({
                    url: URL,
                    type: 'POST',
                    success: function (response) {
                        console.log(response);

                        var title = $(".card-title .badge").html();
                        // $(".card-title .badge").removeClass("badge--success").removeClass("badge--warning").addClass("badge--danger");
                        $(".custom--card .card-header").css('background-color', '#ea5455');
                        $(".card-title .badge").html("Closed" + title.substr(title.indexOf("[")));
                        $(".card-btn").remove();
                        $("#replyForm").remove();

                        // Swal.fire({
                        //     title: 'Close Ticket',
                        //     text: 'Ticket is closed successfully.',
                        //     icon: 'success',
                        //     confirmButtonText: 'OK'
                        // });
                    },
                    error: function (xhr, status, error) {
                        console.log(error);
                        Swal.fire({
                            title: 'Close Ticket',
                            text: error,
                            icon: 'error',
                            confirmButtonText: 'OK'
                        });
                    }
                });
                return true;
            }
            else {
                return false;
            }
        })
    });

    $('#replyForm').submit(function(event) {
        event.preventDefault(); // Prevent default form submission
        
        var formData = new FormData(this);

        console.log(formData);
        // Construct the base URL
        var URL = window.location.protocol + '//' + window.location.host + '/ticket/reply/' + $("input[name=ticketID]").val();
    
        $.ajax({
            url: URL,
            type: 'POST',
            data: formData,
            processData: false,
            contentType: false,
            success: function (response) {
                console.log(response);
                
                var html = "";
                html += 
                    "<div class='fetch-messages'>" +
                        "<div class='row border--success border--rounded my-3 py-3 mx-2'>" + 
                            "<div class='col-md-3 border-end text-end'>" + 
                                "<h5 class='my-3'>" + response.data.name + "</h5>" + 
                            "</div>" + 
                            "<div class='col-md-9'>" + 
                                "<p class='fw-bold my-2'>" + response.data.creationTime + "</p>" + 
                                "<p>" + response.data.message + "</p>" + 
                            "</div>" + 
                        "</div>" +
                    "</div>";

                $($(".fetch-messages")[$(".fetch-messages").length - 1]).append(html);

                var title = $(".card-title .badge").html();
                if (response.data.status == 1) {
                    $(".custom--card .card-header").css('background-color', '#ff9f43');
                    // $(".card-title .badge").removeClass("badge--success").addClass("badge--warning");
                    $(".card-title .badge").html("Suspended" + title.substr(title.indexOf("[")));
                } else if (response.data.status == 2) {
                    $(".custom--card .card-header").css('background-color', '#28c76f');
                    // $(".card-title .badge").removeClass("badge--warning").addClass("badge--success");
                    $(".card-title .badge").html("Open" + title.substr(title.indexOf("[")));
                }
                Swal.fire({
                    title: 'Send Message',
                    text: 'Message is sent.',
                    icon: 'success',
                    confirmButtonText: 'OK'
                });
    
                $('#replyForm')[0].reset();
            },
            error: function (xhr, status, error) {
                console.log(error);
                Swal.fire({
                    title: 'Send Message',
                    text: error,
                    icon: 'error',
                    confirmButtonText: 'OK'
                });
            }
        });
    });  
});

function changeBannerColor(tickets) {
    console.log(tickets);

    if (tickets[0].status == 1) {
        $(".custom--card .card-header").css('background-color', '#ff9f43');
    } else if (tickets[0].status == 2) {
        $(".custom--card .card-header").css('background-color', '#28c76f');
    } else {
        $(".custom--card .card-header").css('background-color', '#ea5455');
    }
}