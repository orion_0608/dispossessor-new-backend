$('#contactForm').submit(function(event) {
    event.preventDefault(); // Prevent default form submission
    
    var formData = new FormData(this);

    // Construct the base URL
    var URL = window.location.protocol + '//' + window.location.host + '/contact/sendmsg';

    $.ajax({
        url: URL,
        type: 'POST',
        data: formData,
        processData: false,
        contentType: false,
        success: function (response) {
            console.log(response);
            if (response.message == 'sign-up') {
                location.reload();
                return;
            }

            var ticket = localStorage.getItem('ticket');
            var ticketList = [];
            if (ticket) {
                ticketList = $.parseJSON(ticket);    
            }
            ticketList.push(response.ticketID);
            localStorage.setItem('ticket', JSON.stringify(ticketList));
            
            Swal.fire({
                title: 'Send Message',
                text: 'Message is sent.',
                icon: 'success',
                confirmButtonText: 'OK'
            });

            $('#contactForm')[0].reset();
        },
        error: function (xhr, status, error) {
            console.log(error);
            Swal.fire({
                title: 'Send Message',
                text: error,
                icon: 'error',
                confirmButtonText: 'OK'
            });
        }
    });
});
