$().ready(function () {
  fetchTickets();
});

function onCloseTicket(ticketID) {
  Swal.fire({
    title: "Confirmation",
    text: "Are you sure you want to close this support ticket?",
    icon: "question",
    showCancelButton: true,
    confirmButtonColor: "#3085d6",
    cancelButtonColor: "#d33",
    confirmButtonText: "Yes",
    cancelButtonText: "No",
  }).then((result) => {
    if (result.isConfirmed) {
      // var ticketID = $(this).data("id");
      // Construct the base URL
      var URL =
        window.location.protocol +
        "//" +
        window.location.host +
        "/ticket/close/" +
        ticketID;

      $.ajax({
        url: URL,
        type: "POST",
        success: function (response) {
          console.log(response);

          var title = $(".ticket" + ticketID + " .card-title").html();
          var statusColor = "#ea5455",
            statusText = "Closed";
          var last = title.substr(title.indexOf("["));
          var first =
            "<a href='/ticket/" +
            ticketID +
            "'>" +
            "<span style='color: " +
            statusColor +
            "'>" +
            statusText;
          var html = first + last;
          $(".ticket" + ticketID + " .card-title").html(html);
          $("#closeCard" + ticketID).remove();
          // $(".card-btn").remove();
        },
        error: function (xhr, status, error) {
          console.log(error);
          Swal.fire({
            title: "Close Ticket",
            text: error,
            icon: "error",
            confirmButtonText: "OK",
          });
        },
      });
      return true;
    } else {
      return false;
    }
  });
}

function fetchTickets() {
  // Construct the base URL
  var URL =
    window.location.protocol + "//" + window.location.host + "/tickets/getList";

  var ticket = localStorage.getItem("ticket");
  var ticketList = [];
  if (ticket) {
    ticketList = $.parseJSON(ticket);
  }

  $.ajax({
    url: URL,
    type: "POST",
    data: { ticket: ticketList },
    success: function (response) {
      var html = "";
      for (var ticketIdx in response.ticketList) {
        var ticket = response.ticketList[ticketIdx];
        var createDate = ticket.creationTime;
        var statusColor = "#ea5455",
          statusText = "Closed";
        if (ticket.status == 1) {
          statusColor = "#ff9f43";
          statusText = "Suspended";
        } else if (ticket.status == 2) {
          statusColor = "#28c76f";
          statusText = "Open";
        }
        if (ticket.status == 0) {
          html +=
            "<div class='ticket ticket" +
            ticket.ticketID +
            "'>" +
            "<div class='title d-flex flex-wrap align-items-center justify-content-between'>" +
            "<div class='card-title'>" +
            "<a href='/ticket/" +
            ticket.ticketID +
            "'>" +
            "<span style='color: " +
            statusColor +
            "'>" +
            statusText +
            "[Ticket#" +
            ticket.ticketID +
            "]</span> " +
            ticket.subject +
            "</a>" +
            "</div>" +
            "</div>" +
            "<div class='date'>" +
            ticket.name +
            ", " +
            ticket.email +
            ", " +
            createDate +
            "</div>" +
            "</div>";
        } else {
          html +=
            "<div class='ticket ticket" +
            ticket.ticketID +
            "'>" +
            "<div class='title d-flex flex-wrap align-items-center justify-content-between'>" +
            "<div class='card-title'>" +
            "<a href='/ticket/" +
            ticket.ticketID +
            "'>" +
            "<span style='color: " +
            statusColor +
            "'>" +
            statusText +
            "[Ticket#" +
            ticket.ticketID +
            "]</span> " +
            ticket.subject +
            "</a>" +
            "</div>" +
            "<div class='card-btn' id='closeCard" +
            ticket.ticketID +
            "'>" +
            "<button type='button' title='Close Ticket'name='closeTicket' class='btn btn--danger text-white border--rounded close-button'" +
            "onclick='onCloseTicket(" +
            ticket.ticketID +
            ")'>" +
            "<i aria-hidden='true' class='fa fa-lg fa-times'></i>" +
            "</button>" +
            "</div>" +
            "</div>" +
            "<div class='date'>" +
            ticket.name +
            ", " +
            ticket.email +
            ", " +
            createDate +
            "</div>" +
            "</div>";
        }
      }
      $("#ticketList").html(html);
    },
    error: function (xhr, status, error) {
      console.log(error);
      Swal.fire({
        title: "Tickets",
        text: "Failed to retrieve tickets!",
        icon: "error",
        confirmButtonText: "OK",
      });
    },
  });
}
