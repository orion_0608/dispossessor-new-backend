const fs = require("fs");
const readline = require("readline");
var connection = require("./database");

var companies_id = [];

connection.connect((err) => {
  if (err) {
    console.error("Error connecting to the database:", err);
    return;
  }
  console.log("Connected to the database.");
});

// Update data table from list_of_companies.txt file
function updateDataFromTxt() {
  const fileStream = fs.createReadStream("../list_of_companies.txt");

  const rl = readline.createInterface({
    input: fileStream,
    crlfDelay: Infinity,
  });

  rl.on("line", (line) => {
    let number = parseInt(line.split("_")[0]);
    companies_id.push(number);
  });

  rl.on("close", () => {
    companies_id.sort((a, b) => a - b);

    // Construct the SQL query to delete records based on the condition
    const delete_sql = `DELETE FROM data WHERE id NOT IN (${companies_id.join(
      ","
    )})`;

    // Execute the delete query
    connection.query(delete_sql, (error, results, fields) => {
      if (error) {
        console.error("Error deleting records:", error);
      } else {
        console.log("Records deleted successfully:", results.affectedRows);
      }
    });
  });
}

//  Get diff company name between data table and list_of_companies.txt
function getDiffCompanies() {
  // Define an array to store IDs
  let idsArray = [];

  let diffValues = [];

  // Construct the SQL query to select IDs from the table
  const select_sql = "SELECT id FROM data";

  // Execute the select query
  connection.query(select_sql, (error, results, fields) => {
    if (error) {
      console.error("Error fetching IDs:", error);
    } else {
      // Extract IDs from the results and store them in the array
      idsArray = results.map((row) => row.id);

      for (const num of companies_id) {
        if (!idsArray.includes(num)) {
          diffValues.push(num);
        }
      }
      const fileStream = fs.createReadStream("../list_of_companies.txt");

      const rl = readline.createInterface({
        input: fileStream,
        crlfDelay: Infinity,
      });

      miss_companies = [];

      rl.on("line", (line) => {
        let number = parseInt(line.split("_")[0]);
        for (const num of diffValues) {
          if (num == number) {
            miss_companies.push(line);
          }
        }
      });

      rl.on("close", (line) => {
        console.log(miss_companies);
      });
    }
  });
}

updateDataFromTxt();
getDiffCompanies();
