const axios = require("axios");

async function sendEmailViaPostmark(data) {
  try {
    const config = {
      method: "post",
      maxBodyLength: Infinity,
      url: "https://api.postmarkapp.com/email",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        "X-Postmark-Server-Token": "f713db37-125a-4660-bca7-97b6145fa608",
      },
      data: data,
    };

    const response = await axios.request(config);
    return response.data;
  } catch (error) {
    console.error("error");
    throw error;
  }
}

module.exports = sendEmailViaPostmark;
