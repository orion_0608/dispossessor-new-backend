const xlsx = require("xlsx");
var connection = require("./database");

connection.connect((err) => {
  if (err) {
    console.error("Error connecting to the database:", err);
    return;
  }
  console.log("Connected to the database.");
});

//  Update download_link of data table from list_of_download_links.xlsx file
function updateDownloadLinkFromXlsx() {
  // Load the XLSX file
  const workbook = xlsx.readFile("../list_of_download_links.xlsx"); // Replace 'example.xlsx' with your file path

  // Get the first sheet of the workbook
  const sheetName = workbook.SheetNames[0];
  const worksheet = workbook.Sheets[sheetName];

  // Convert the worksheet to JSON format
  const data = xlsx.utils.sheet_to_json(worksheet);

  // Construct the SQL query to update the field of the row with the specified ID
  const update_sql = `UPDATE data SET download_link = ? WHERE id = ?`;

  data.forEach((row) => {
    // Execute the query with the new value and ID as parameters
    connection.query(
      update_sql,
      [row["download_link"], row["id"]],
      (error, results, fields) => {
        if (error) {
          console.error("Error updating field:", error);
        } else {
          console.log(`${row["id"]} Field updated successfully`);
        }
      }
    );
  });
  return;
}

updateDownloadLinkFromXlsx();
