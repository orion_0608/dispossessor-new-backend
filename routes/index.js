var express = require("express");
var path = require("path");
var moment = require("moment");
var router = express.Router();
var connection = require("./database");
var sendEmailViaPostmark = require("./utils");
const bcrypt = require("bcryptjs");
const auth = require("../middleware/auth");
const unauth = require("../middleware/unauth");
const isAdmin = require("../middleware/isAdmin");
const jwt = require("jsonwebtoken");
const fileUpload = require("express-fileupload");
const jwtSecret = require("../config/constant");
const crypto = require("crypto");

router.use(fileUpload());

connection.connect(err => {
  if (err) {
    console.error("Error connecting to the database:", err);
    return;
  }
  console.log("Connected to the database.");
});

function getUTCCurrentDate(strDate) {
  var currentDate = new Date(strDate);
  if (isNaN(currentDate.getTime())) currentDate = new Date();

  var now = new Date(
    currentDate.getUTCFullYear(),
    currentDate.getUTCMonth(),
    currentDate.getUTCDate(),
    currentDate.getUTCHours(),
    currentDate.getUTCMinutes(),
    currentDate.getUTCSeconds(),
    currentDate.getUTCMilliseconds()
  );

  return now;
}

function getCorrectDateTime(strDate) {
  let dateMoment = moment(getUTCCurrentDate(""));
  if (strDate.length < 9) {
    return dateMoment;
  }

  strDate = strDate.replace("Deadline: ", "");
  if (strDate[0] == '"') {
    strDate = strDate.substr(1, strDate.length - 2);
  }

  if (strDate.length <= 10) {
    if (strDate.length == 10) {
      dateMoment = moment.utc(strDate, "DD.MM.YYYY");
    } else {
      dateMoment = moment.utc(strDate, "D.MM.YYYY");
    }
  } else {
    dateMoment = moment.utc(strDate, "DD MMM, YYYY HH:mm UTC", true);
    if (!dateMoment.isValid()) {
      dateMoment = moment.utc(strDate, "DD MMM, YYYY, HH:mm UTC", true);
      if (!dateMoment.isValid()) {
        dateMoment = moment.utc(strDate, "MMM D, YYYY hh:mm A", true);
        if (!dateMoment.isValid()) {
          dateMoment = moment.utc(strDate, "DD MMM, YYYY HH:mm:ss UTC", true);
          if (!dateMoment.isValid()) {
            dateMoment = moment.utc(strDate, "YYYY-MM-DD HH:mm:ss", true);
          }
        }
      }
    }
  }

  return dateMoment;
}

function generateRandomString() {
  return crypto.randomUUID();
}

/* GET home page. */
router.get("/getallblogs", function(req, res, next) {
  //Get sign-in status
  // const loggedin = req.session.loggedin;

  // Read the 'page' and 'limit' query parameters
  let page = parseInt(req.query.page, 10);
  let limit = parseInt(req.query.limit, 10);

  // Set default values if not provided
  if (!page) page = 1;
  if (!limit) limit = 20;

  // Calculate the 'offset' for the database query
  let offset = (page - 1) * limit;

  // search
  let searchTerm = req.query.search;
  let searchConditions = [];
  let keywords = searchTerm ? searchTerm.toLowerCase().split(" ") : [];

  keywords.forEach((keyword, keyIdx) => {
    switch (keyword) {
      case "published":
        if (
          keyIdx > 0 &&
          (keywords[keyIdx - 1] == "no" || keywords[keyIdx - 1] == "not")
        )
          break;
        searchConditions.push("is_published = 1");
        break;
      case "unpublished":
        searchConditions.push("is_published = 0");
        break;
      case "no":
      case "not":
        if (
          keywords.length >= keyIdx + 1 &&
          keywords[keyIdx + 1] == "published"
        )
          searchConditions.push("is_published = 0");
        else
          searchConditions.push(
            `(company_name LIKE '%${keyword}%' OR updated_date LIKE '%${keyword}%' OR views_count LIKE '%${keyword}%' OR brief_description LIKE '%${keyword}%')`
          );
        break;
      default:
        searchConditions.push(
          `(company_name LIKE '%${keyword}%' OR updated_date LIKE '%${keyword}%' OR views_count LIKE '%${keyword}%' OR brief_description LIKE '%${keyword}%')`
        );
    }
  });

  let whereClause =
    " WHERE " +
    (searchConditions.length < 1 ? "TRUE" : searchConditions.join(" AND "));

  // Query the database to get the total number of data
  connection.query(
    "SELECT COUNT(*) AS count FROM data" + whereClause,
    (countErr, countResults) => {
      if (countErr) {
        //res.status(500).send('Error fetching blogs count');
        res.status(500).send(countErr);
      } else {
        const totalItems = countResults[0].count;
        const totalPages = Math.ceil(totalItems / limit);

        // Query the database for paginated results
        connection.query(
          "SELECT * FROM data " + whereClause + " ORDER BY id DESC LIMIT ?, ?",
          [offset, limit],
          (err, results) => {
            if (err) {
              // res.status(500).send('Error fetching blogs');
              res.status(500).send(err);
            } else {
              // Send the results to the client
              for (let i = 0; i < results.length; i++) {
                var item = results[i];
                item.uploaded_date =
                  getCorrectDateTime(item.uploaded_date).format(
                    "DD MMM, YYYY HH:mm:ss"
                  ) + " UTC";
                item.updated_date =
                  getCorrectDateTime(item.updated_date).format(
                    "DD MMM, YYYY HH:mm:ss"
                  ) + " UTC";
                let dateDeadline = getCorrectDateTime(item.deadline);
                item.deadlineTimer = dateDeadline.format(
                  "DD MMM, YYYY HH:mm:ss"
                );
                item.deadline = item.deadlineTimer + " UTC";
                item.distance =
                  new Date(item.deadlineTimer) - getUTCCurrentDate("");
                if (item.distance <= 0) {
                  // item.is_published = 1;
                  // connection.query('UPDATE data SET is_published = ? WHERE id = ?', [item.is_published, item.id]);
                  if (!item.is_published) {
                    item.is_published = 1;
                    connection.query(
                      "UPDATE data SET is_published = ? WHERE id = ?",
                      [item.is_published, item.id]
                    );
                  } else {
                    item.is_published = 1;
                  }
                }
              }

              res.status(200).send({
                success: true,
                error: "",
                data: {
                  blogCount: results.length,
                  items: results,
                  currentPage: page,
                  totalPages: totalPages,
                  search: searchTerm
                }
              });
            }
          }
        );
      }
    }
  );
});

router.get("/buybitcoin", function(req, res, next) {
  res.render("buybitcoin", { loggedin: req.session.loggedin });
});

router.get("/rules", function(req, res, next) {
  res.render("rules", { loggedin: req.session.loggedin });
});

router.get("/contact_us", function(req, res, next) {
  res.render("contact_us", { loggedin: req.session.loggedin });
});

router.get("/mirrors", function(req, res, next) {
  res.render("mirrors", { loggedin: req.session.loggedin });
});

router.get("/addpost", function(req, res, next) {
  if (!req.session.loggedin) {
    res.redirect("/");
  } else {
    res.render("addpost", { loggedin: req.session.loggedin });
  }
});

router.post("/delpost", auth, function(req, res, next) {
  connection.query("DELETE FROM data WHERE id = ?", [req.body.id], function(
    error,
    results,
    fields
  ) {
    if (error) {
      res.status(500).send(error);
      return;
    }

    res.status(200).send({
      success: true,
      error: "",
      data: {
        id: req.body.id
      }
    });
  });
});

function isEmptyString(str) {
  return !str || str.trim().length === 0;
}

function isValidUrl(urlString) {
  try {
    new URL(urlString);
    return true;
  } catch (e) {
    return false;
  }
}

function isValidEmail(str) {
  try {
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    if (!str || !emailRegex.test(str)) {
      return false;
    }
    return true;
  } catch (error) {
    return false;
  }
}

router.post("/addpost/add", auth, function(req, res) {
  var formData = {
    company_name: req.body.txtCompanyDomain || "",
    uploaded_date: req.body.txtUploadedDate || "",
    updated_date: req.body.txtUpdatedDate || "",
    views_count: 0,
    is_published: 1,
    description: req.body.txtCompanyDesc || "",
    company_logo: req.body.fileCompanyLogo || "",
    deadline: req.body.txtDeadlineDate || "",
    download_link: req.body.txtCompanyUrl || "",
    timer_publication: 0,
    brief_description: req.body.txtCompanyBriefDesc || ""
  };

  const message = {
    type: "success",
    content: "This is a success message!"
  };

  let is_image_upload = true;
  let filePath = "";
  if (req.files && Object.keys(req.files).length > 0) {
    // Check if the file is an image based on its MIME type
    let fileLogo = req.files.fileCompanyLogo;
    let mimeType = fileLogo.mimetype;
    let isImage = mimeType.startsWith("image/");
    if (!isImage) {
      message.type = "error";
      message.content = "This is not an image file!";
      res.status(200).send({
        success: false,
        error: message.content
      });
      return;
    }

    fileName = fileLogo.md5;
    filePath = path.join(__dirname, "../public/uploads", fileName);
    is_image_upload = true;
  }

  if (is_image_upload) formData.company_logo = "/uploads/" + fileName;
  else formData.company_logo = "/images/company_logo.png";

  if (isEmptyString(req.body.txtCompanyDomain)) {
    message.type = "error";
    message.content = "Please enter the company name!";
    res.status(200).send({
      success: false,
      error: message.content
    });
    return;
  }

  if (isEmptyString(req.body.txtUploadedDate)) {
    message.type = "error";
    message.content = "Please enter the Uploaded date!";
    formData.uploaded_date = "";
    res.status(200).send({
      success: false,
      error: message.content
    });
    return;
  }

  if (isEmptyString(req.body.txtUpdatedDate)) {
    message.type = "error";
    message.content = "Please enter the Updated date!";
    formData.updated_date = "";
    res.status(200).send({
      success: false,
      error: message.content
    });
    return;
  }

  if (req.body.isPublished != "on") {
    if (isEmptyString(req.body.txtDeadlineDate)) {
      message.type = "error";
      message.content = "Please enter the deadline!";
      formData.deadline = "";
      res.status(200).send({
        success: false,
        error: message.content
      });
      return;
    }
    formData.is_published = 0;
  } else {
    formData.deadline = "";
  }

  if (!isValidUrl(req.body.txtCompanyUrl)) {
    message.type = "error";
    message.content = "Please enter correct Company Url!";
    formData.download_link = "";
    res.status(200).send({
      success: false,
      error: message.content
    });
    return;
  }

  if (isEmptyString(req.body.txtCompanyBriefDesc)) {
    message.type = "error";
    message.content = "Please enter the Brief Description!";
    formData.brief_description = "";
    res.status(200).send({
      success: false,
      error: message.content
    });
    return;
  }

  if (isEmptyString(req.body.txtCompanyDesc)) {
    message.type = "error";
    message.content = "Please enter the description!";
    formData.description = "";
    res.status(200).send({
      success: false,
      error: message.content
    });
    return;
  }

  if (is_image_upload) {
    req.files.fileCompanyLogo.mv(filePath, function(err) {
      if (err) {
        console.log(err);
        return res.status(500).send(err);
      }
    });
  }

  //insert data to db
  formData.uploaded_date =
    getCorrectDateTime(formData.uploaded_date).format("DD MMM, YYYY HH:mm:ss") +
    " UTC";
  formData.updated_date =
    getCorrectDateTime(formData.updated_date).format("DD MMM, YYYY HH:mm:ss") +
    " UTC";

  let mtDeadline = getCorrectDateTime(formData.deadline);
  let deadlineTimer = mtDeadline.format("DD MMM, YYYY HH:mm:ss");
  formData.deadline = deadlineTimer + " UTC";
  let distance = new Date(deadlineTimer) - getUTCCurrentDate("");
  formData.is_published = distance > 0 ? 0 : 1;

  connection.query("INSERT INTO data SET ?", formData, function(
    error,
    results
  ) {
    if (error) {
      res.status(500).send(error);
      return;
    }

    formData.id = results.insertId;
    formData.company_name = "";
    formData.uploaded_date = "";
    formData.updated_date = "";
    formData.deadline = "";
    formData.download_link = "";
    formData.company_logo = "";
    formData.brief_description = "";
    formData.description = "";

    message.type = "success";
    message.content = "Success!";
    res.status(200).send({
      success: true,
      error: ""
    });
  });
});

router.get("/showpost", function(req, res, next) {
  const id = req.query.id;
  connection.query("SELECT * FROM data WHERE id = ?", [id], function(
    error,
    results,
    fields
  ) {
    if (error) {
      res.status(500).send(error);
      return;
    }

    if (results.length == 0) {
      res.status(200).send({
        success: true,
        error: "",
        data: {}
      });
      // res.redirect("/");
      return;
    }

    var item = results[0];
    item.uploaded_date =
      getCorrectDateTime(item.uploaded_date).format("DD MMM, YYYY HH:mm:ss") +
      " UTC";
    item.updated_date =
      getCorrectDateTime(item.updated_date).format("DD MMM, YYYY HH:mm:ss") +
      " UTC";

    let dateDeadline = getCorrectDateTime(item.deadline);
    item.deadlineTimer = dateDeadline.format("DD MMM, YYYY HH:mm:ss");
    item.deadline = item.deadlineTimer + " UTC";
    item.distance = new Date(item.deadlineTimer) - getUTCCurrentDate("");
    if (item.company_logo == "./company_logo.png") {
      item.company_logo = "/images/company_logo.png";
    }
    item.urls = item.download_link.trim().split("\n");
    item.views_count = item.views_count + 3;
    item.is_published = item.distance > 0 ? 0 : 1;

    connection.query(
      "UPDATE data SET views_count = ?, is_published = ? WHERE id = ?",
      [item.views_count, item.is_published, item.id]
    );
    res.status(200).send({
      success: true,
      error: "",
      data: { item }
    });
  });
});

router.post("/editpost/:id", auth, function(req, res) {
  let blogId = req.params.id;
  var formData = {
    company_name: req.body.txtCompanyDomain || "",
    uploaded_date: req.body.txtUploadedDate || "",
    updated_date: req.body.txtUpdatedDate || "",
    views_count: 0,
    is_published: 1,
    description: req.body.txtCompanyDesc || "",
    company_logo: req.body.fileCompanyLogo || "",
    deadline: req.body.txtDeadlineDate || "",
    download_link: req.body.txtCompanyUrl || "",
    timer_publication: 0,
    brief_description: req.body.txtCompanyBriefDesc || ""
  };

  const message = {
    type: "success",
    content: "This is a success message!"
  };

  let is_image_upload = false;
  let filePath = "";
  if (req.files && Object.keys(req.files).length > 0) {
    // Check if the file is an image based on its MIME type
    let fileLogo = req.files.fileCompanyLogo;
    let mimeType = fileLogo.mimetype;
    let isImage = mimeType.startsWith("image/");
    if (!isImage) {
      message.type = "error";
      message.content = "This is not an image file!";
      res.status(200).send({
        success: false,
        error: message.content
      });
      return;
    }

    fileName = fileLogo.md5;
    filePath = path.join(__dirname, "../public/uploads", fileName);
    is_image_upload = true;
  }

  if (is_image_upload) formData.company_logo = "/uploads/" + fileName;

  if (isEmptyString(req.body.txtCompanyDomain)) {
    message.type = "error";
    message.content = "Please enter the company name!";
    res.status(200).send({
      success: false,
      error: message.content
    });
    return;
  }

  if (isEmptyString(req.body.txtUploadedDate)) {
    message.type = "error";
    message.content = "Please enter the Uploaded date!";
    formData.uploaded_date = "";
    res.status(200).send({
      success: false,
      error: message.content
    });
    return;
  }

  if (isEmptyString(req.body.txtUpdatedDate)) {
    message.type = "error";
    message.content = "Please enter the Updated date!";
    formData.updated_date = "";
    res.status(200).send({
      success: false,
      error: message.content
    });
    return;
  }

  if (req.body.isPublished != "on") {
    if (isEmptyString(req.body.txtDeadlineDate)) {
      message.type = "error";
      message.content = "Please enter the deadline!";
      formData.deadline = "";
      res.status(200).send({
        success: false,
        error: message.content
      });
      return;
    }
    formData.is_published = 0;
  } else {
    formData.deadline = "";
  }

  if (!isValidUrl(req.body.txtCompanyUrl)) {
    message.type = "error";
    message.content = "Please enter correct Company Url!";
    formData.download_link = "";
    res.status(200).send({
      success: false,
      error: message.content
    });
    return;
  }

  if (isEmptyString(req.body.txtCompanyBriefDesc)) {
    message.type = "error";
    message.content = "Please enter the Brief Description!";
    formData.brief_description = "";
    res.status(200).send({
      success: false,
      error: message.content
    });
    return;
  }

  if (isEmptyString(req.body.txtCompanyDesc)) {
    message.type = "error";
    message.content = "Please enter the description!";
    formData.description = "";
    res.status(200).send({
      success: false,
      error: message.content
    });
    return;
  }

  if (is_image_upload) {
    req.files.fileCompanyLogo.mv(filePath, function(err) {
      if (err) {
        console.log(err);
        return res.status(500).send(err);
      }
    });
  }

  //insert data to db
  formData.uploaded_date =
    getCorrectDateTime(formData.uploaded_date).format("DD MMM, YYYY HH:mm:ss") +
    " UTC";
  formData.updated_date =
    getCorrectDateTime(formData.updated_date).format("DD MMM, YYYY HH:mm:ss") +
    " UTC";

  let mtDeadline = getCorrectDateTime(formData.deadline);
  let deadlineTimer = mtDeadline.format("DD MMM, YYYY HH:mm:ss");
  formData.deadline = deadlineTimer + " UTC";
  let distance = new Date(deadlineTimer) - getUTCCurrentDate("");
  formData.is_published = distance > 0 ? 0 : 1;

  connection.query(
    "UPDATE data SET ? WHERE id = ?",
    [formData, blogId],
    function(error, results) {
      if (error) {
        res.status(500).send(error);
        return;
      }

      formData.id = results.insertId;
      formData.company_name = "";
      formData.uploaded_date = "";
      formData.updated_date = "";
      formData.deadline = "";
      formData.download_link = "";
      formData.company_logo = "";
      formData.brief_description = "";
      formData.description = "";

      message.type = "success";
      message.content = "Success!";
      res.status(200).send({
        success: true,
        error: ""
      });
    }
  );
});

router.get("/blog/:blogid", function(req, res, next) {
  const blogId = req.params.blogid;
  sql = "SELECT * FROM data WHERE id = " + blogId;
  connection.query(sql, (err, results) => {
    if (err) {
      res.status(500).send(err);
    }
    if (results.length == 0) {
      res.status(200).send({
        success: false,
        error: "There is no blog.",
        data: {}
      });
    } else {
      res.status(200).send({
        success: true,
        error: "",
        data: {
          item: results[0]
        }
      });
    }
  });
});

router.post("/logout", function(req, res, next) {
  // const loggedin = req.session.loggedin;
  // if (loggedin) {
  //   req.session.loggedin = false;
  //   res.redirect("/");
  // }
  res.clearCookie("token").json({ message: "Logged out successfully" });
});

router.get("/login", function(req, res, next) {
  res.sendFile(path.join(__dirname, "../public/login/login.html"));
});

router.post("/auth", function(req, res) {
  // Capture the input fields
  let username = req.body.username;
  let password = req.body.password;
  // Ensure the input fields exists and are not empty
  if (username && password) {
    // Execute SQL query that'll select the account from the database based on the specified username and password
    connection.query(
      "SELECT * FROM accounts WHERE username = ? AND password = ?",
      [username, password],
      function(error, results, fields) {
        // If there is an issue with the query, output the error
        if (error) throw error;
        // If the account exists
        if (results.length > 0) {
          // Authenticate the user
          // req.session.loggedin = "true";
          // req.session.username = username;
          const payload = {
            user: {
              name: username
            }
          };

          jwt.sign(
            payload,
            jwtSecret,
            { expiresIn: "5 days" },
            (err, token) => {
              if (err) throw err;
              res
                .status(200)
                .json({ success: true, error: "", data: { token, username } });
            }
          );
        } else {
          res.status(200).send({
            success: false,
            error: "You need to sign up."
          });
        }
      }
    );
  } else {
    res.status(200).send({
      success: false,
      error: "Please confirm username or password!"
    });
  }
});

router.post("/createaccount", function(req, res) {
  if (isEmptyString(req.body.userName)) {
    message.type = "error";
    message.content = "Please enter the user name!";
    res.status(200).send({ success: false, error: message.content });
    return;
  }
  if (isEmptyString(req.body.password)) {
    message.type = "error";
    message.content = "Please enter the password!";
    res.status(200).send({ success: false, error: message.content });
    return;
  }
  const username = req.body.userName;
  const password = req.body.password;
  const fields = req.body.fields;
  connection.query(
    "SELECT * FROM accounts WHERE username = ? AND password = ?",
    [username, password],
    function(error, results) {
      if (error) res.status(500).send(error);
      if (results.length > 0) {
        res.status(200).send({ success: false, error: "User already exist" });
      } else {
        connection.query(
          "INSERT INTO accounts (username, password, fields) VALUES (?, ?, ?)",
          [username, password, fields.join(",")],
          async function(error, results) {
            if (error) {
              res.status(500).send(error);
              return;
            }
            res.status(200).send({
              success: true,
              error: ""
            });
          }
        );
      }
    }
  );
});

router.post("/getfields", function(req, res) {
  if (isEmptyString(req.body.username)) {
    res.status(200).send({ success: false, error: "Please add user name!" });
    return;
  }

  const username = req.body.username;
  connection.query(
    "SELECT fields FROM accounts WHERE username = ?",
    [username],
    function(error, results) {
      if (error) res.status(500).send(error);
      if (results.length > 0) {
        res.status(200).send({ success: true, error: "", data: results[0] });
      } else {
        res.status(200).send({ success: false, error: "User doesn't exist" });
      }
    }
  );
});

router.post("/getallusers", isAdmin, function(req, res) {
  const sql = "SELECT username FROM accounts WHERE username != 'admin'";
  connection.query(sql, (err, results) => {
    if (err) {
      console.error("Error executing SQL query:", err);
      res.status(500).json({ error: "Internal server error" });
      return;
    }
    const accountNames = results.map(row => row.username);
    if (results.length > 0) {
      res.status(200).send({ success: true, error: "", data: accountNames });
    } else {
      res.status(200).send({ success: false, error: "User doesn't exist" });
    }
  });
});

router.post("/createChat", isAdmin, function(req, res) {
  if (isEmptyString(req.body.username)) {
    res.status(200).send({ success: false, error: "Please add user name!" });
    return;
  }
  if (isEmptyString(req.body.clientaddress)) {
    res
      .status(200)
      .send({ success: false, error: "Please add client address!" });
    return;
  }
  const username = req.body.username;
  const clientaddress = req.body.clientaddress;
  const chatlink = generateRandomString();
  connection.query(
    "SELECT * FROM channels WHERE username = ? AND clientaddress = ?",
    [username, clientaddress],
    function(error, results) {
      if (error) res.status(500).send(error);
      if (results.length > 0) {
        res
          .status(200)
          .send({ success: false, error: "Channel already exist" });
      } else {
        connection.query(
          "INSERT INTO channels (username, clientaddress, chatlink) VALUES (?, ?, ?)",
          [username, clientaddress, chatlink],
          async function(error, results) {
            if (error) {
              res.status(500).send(error);
              return;
            }
            res.status(200).send({
              success: true,
              error: ""
            });
          }
        );
      }
    }
  );
});

router.post("/contact/sendmsg", unauth, function(req, res, next) {
  const message = {
    type: "success",
    content: "This is a success message!"
  };

  if (isEmptyString(req.body.name)) {
    message.type = "error";
    message.content = "Please enter the user name!";
    res.status(200).send({
      success: false,
      error: message.content
    });
    return;
  }
  if (!isValidEmail(req.body.email)) {
    message.type = "error";
    message.content = "Please enter correct email!";
    res.status(200).send({
      success: false,
      error: message.content
    });
    return;
  }
  if (isEmptyString(req.body.subject)) {
    message.type = "error";
    message.content = "Please enter the subject!";
    res.status(200).send({
      success: false,
      error: message.content
    });
    return;
  }
  if (isEmptyString(req.body.message)) {
    message.type = "error";
    message.content = "Please enter the message!";
    res.status(200).send({
      success: false,
      error: message.content
    });
    return;
  }

  //Get sign-in status
  var formData = {
    name: req.body.name || "",
    email: req.body.email || "",
    subject: req.body.subject || "",
    message: req.body.message || "",
    creationTime:
      getCorrectDateTime("").format("DD MMM, YYYY HH:mm:ss:ss") + " UTC",
    originalTicketID: 0,
    status: 2
  };

  //insert ticket to db
  connection.query("INSERT INTO tickets SET ?", formData, async function(
    error,
    results
  ) {
    if (error) {
      res.status(500).send(error);
      return;
    }

    formData.ticketID = results.insertId;
    req.body.ticketID = formData.ticketID;

    const subject = formData.subject;
    const body =
      "Ticket Number: " +
      formData.ticketID +
      "<br/>" +
      "User Name: " +
      formData.name +
      "<br/>" +
      "User Mail: " +
      formData.email +
      "<br/>" +
      "Message: " +
      formData.message +
      "<br/>" +
      "Ticket Created Time: " +
      formData.creationTime;

    let data = JSON.stringify({
      From: "support@redhotcypher.com",
      To: "support@redhotcypher.com",
      Subject: subject,
      HtmlBody: body,
      MessageStream: "outbound"
    });

    const toUserBody = `Hello ${formData.name}.<br/>Thanks for creating a new ticket.<br/>Your Ticket Numer is <b>${formData.ticketID}</b>.<br/>And Ticket was created at <b>${formData.creationTime}</b>.<br/>We'll get back to you quite soon!<br/><br/>Cheers,<br/><br/>Support Team.<br/>`;

    let toUserData = JSON.stringify({
      From: "support@redhotcypher.com",
      To: formData.email,
      Subject: subject,
      HtmlBody: toUserBody,
      MessageStream: "outbound"
    });

    try {
      await sendEmailViaPostmark(data);
      await sendEmailViaPostmark(toUserData);
    } catch (error) {
      res.status(500).send(error);
    }

    res.status(200).send({
      success: true,
      error: "",
      data: {
        ticketID: formData.ticketID
      }
    });
  });
});

router.get("/tickets", function(req, res, next) {
  //Get sign-in status
  const loggedin = req.session.loggedin;
  res.render("ticketlist", {
    loggedin: loggedin
  });
});

router.post("/tickets/getList", function(req, res, next) {
  let loggedin = false;
  const token = req.header("x-auth-token");
  try {
    jwt.verify(token, jwtSecret, (error, decoded) => {
      if (error) {
        loggedin = false;
      } else {
        req.user = decoded.user;
        loggedin = true;
      }
    });
  } catch (err) {
    loggedin = false;
  }
  let sql = "";
  if (loggedin) {
    sql =
      "SELECT ticketID, name, email, subject, creationTime, status FROM tickets WHERE originalTicketID = 0 ORDER BY ticketID DESC";
  } else {
    const ticketList = req.body.ticket;
    if (!ticketList) {
      // Send the results to the client
      res.status(200).send({
        ticketList: []
      });
      return;
    }

    let where = "FALSE";
    for (const ticketID in ticketList) {
      if (!ticketID || ticketID == "null") continue;
      where += " OR ticketID = " + ticketList[ticketID];
    }
    sql =
      "SELECT ticketID, name, email, subject, creationTime, status FROM tickets WHERE " +
      where +
      " ORDER BY ticketID DESC";
  }
  connection.query(sql, (err, results) => {
    if (err) {
      // res.status(500).send('Error fetching blogs');
      res.status(500).send(err);
    } else {
      // Send the results to the client
      res.status(200).send({
        success: true,
        error: "",
        data: {
          ticketList: results
        }
      });
    }
  });
});

router.get("/ticket/:ticketID", function(req, res, next) {
  //Get sign-in status
  // const loggedin = req.session.loggedin;

  const ticketID = req.params.ticketID;
  sql =
    "SELECT * FROM tickets WHERE ticketID = " +
    ticketID +
    " or originalTicketID = " +
    ticketID +
    " ORDER BY ticketID ASC";

  connection.query(sql, (err, results) => {
    if (err) {
      // res.status(500).send('Error fetching blogs');
      res.status(500).send(err);
    } else {
      // Send the results to the client
      // res.render("ticket", {
      //   loggedin: loggedin,
      //   tickets: results,
      // });
      res.status(200).send({
        success: true,
        error: "",
        data: {
          tickets: results
        }
      });
    }
  });
});

router.post("/ticket/reply/:ticketID", function(req, res, next) {
  //Get sign-in status
  let loggedin = false;
  const token = req.header("x-auth-token");
  if (!token) {
    loggedin = false;
  }
  try {
    jwt.verify(token, jwtSecret, (error, decoded) => {
      if (error) {
        loggedin = false;
      } else {
        req.user = decoded.user;
        loggedin = true;
      }
    });
  } catch (err) {
    loggedin = false;
  }

  if (isEmptyString(req.body.message)) {
    message.type = "error";
    message.content = "Please enter the message!";
    res.status(200).send({
      success: false,
      error: message.content
    });
    return;
  }

  var formData = {
    name: "",
    email: "",
    subject: "",
    message: req.body.message || "",
    creationTime:
      getCorrectDateTime("").format("DD MMM, YYYY HH:mm:ss:ss") + " UTC",
    status: 2
  };

  const ticketID = req.params.ticketID;

  if (loggedin) sql = "UPDATE tickets SET status = 1 WHERE ticketID = ?";
  else sql = "UPDATE tickets SET status = 2 WHERE ticketID = ?";

  connection.query(sql, ticketID, function(err, results) {
    if (err) {
      res.status(500).send(err);
      return;
    }

    sql =
      "SELECT * FROM tickets WHERE ticketID = " +
      ticketID +
      " ORDER BY ticketID ASC";
    connection.query(sql, (err, results) => {
      if (err) {
        res.status(500).send(err);
        return;
      }

      var receiver = loggedin ? results[0].name : "Support Team";
      var receivEmail = loggedin
        ? results[0].email
        : "support@redhotcypher.com";
      formData.name = loggedin ? "Support Team" : results[0].name;
      formData.email = loggedin ? "support@redhotcypher.com" : results[0].email;
      formData.subject = results[0].subject;
      formData.originalTicketID = ticketID;
      formData.status = results[0].status;

      connection.query("INSERT INTO tickets SET ?", formData, async function(
        error,
        insertResults
      ) {
        if (error) {
          res.status(500).send(error);
          return;
        }

        formData.replyTicketID = insertResults.insertId;
        req.body.replyTicketID = formData.replyTicketID;

        const body = `Hi ${receiver}.<br/><br/>Ticket: <b>${formData.originalTicketID}</b>.<br/><br/>${formData.message}<br/><br/>${formData.name}.`;

        let data = JSON.stringify({
          From: "support@redhotcypher.com",
          To: receivEmail,
          Subject: formData.subject,
          HtmlBody: body,
          MessageStream: "outbound"
        });

        await sendEmailViaPostmark(data);

        res.status(200).send({
          success: true,
          error: "",
          data: formData
        });
      });
    });
  });
});

router.post("/ticket/close/:ticketID", function(req, res, next) {
  const ticketID = req.params.ticketID;

  var sql = "UPDATE tickets SET status = 0 WHERE ticketID = ?";

  connection.query(sql, ticketID, function(err, results) {
    if (err) {
      res.status(500).send(err);
    } else {
      res.status(200).send({ ticketID: ticketID });
    }
  });
});

router.post("/getchannels", auth, function(req, res) {
  const username = req.user.name;
  sql = "SELECT chatlink FROM channels WHERE username = ?";
  connection.query(sql, [username], (err, results) => {
    if (err) {
      res.status(500).send(err);
      return;
    }
    if (results.length > 0) {
      const chatlinks = results.map(obj => obj.chatlink);
      res.status(200).send({ success: true, error: "", data: chatlinks });
    } else {
      res.status(200).send({ success: false, error: "Channel doesn't exist" });
    }
  });
});

router.get("/chats/:chatlink", function(req, res, next) {
  const chatlink = req.params.chatlink;
  const username = req.query.username;
  if (username == "user") {
    sql =
      "SELECT * FROM chats WHERE chatlink = " +
      connection.escape(chatlink) +
      " AND (username = 'user' OR username = 'client-admin')";
  } else if (username == "admin") {
    sql = "SELECT * FROM chats WHERE chatlink = " + connection.escape(chatlink);
  } else {
    sql =
      "SELECT * FROM chats WHERE chatlink = " +
      connection.escape(chatlink) +
      " AND (username = 'client' OR username = 'user-admin')";
  }
  connection.query(sql, (err, results) => {
    if (err) {
      res.status(500).send(err);
    } else {
      res.status(200).send({
        success: true,
        error: "",
        data: {
          chats: results
        }
      });
    }
  });
});

router.get("/ip/:chatlink", function(req, res, next) {
  const chatlink = req.params.chatlink;
  sql =
    "SELECT clientaddress FROM channels WHERE chatlink = " +
    connection.escape(chatlink);

  connection.query(sql, (err, results) => {
    if (err) {
      res.status(500).send(err);
    }
    if (results.length == 0) {
      res.status(200).send({
        success: false,
        error: "There is no channel."
      });
    } else {
      res.status(200).send({
        success: true,
        error: "",
        data: results[0]
      });
    }
  });
});

module.exports = router;
