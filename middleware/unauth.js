const jwt = require("jsonwebtoken");
const jwtSecret = require("../config/constant");

module.exports = function (req, res, next) {
  // Get token from header
  const token = req.header("x-auth-token");

  // Check if not token
  if (token) {
    return res.status(401).json({ msg: "You should log out!" });
  }

  next();
};
